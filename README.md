# sonarr container


### docker-compose.yml

```
  sonarr:
    image: registry.gitlab.com/daemonp/sonarr
    ports:
      - "8989:8989"
    volumes:
      - "/share/data/:/data"
      - "/etc/localtime:/etc/localtime:ro"
    restart: always

```
