FROM debian:buster-slim

# mono 3.10 currently doesn't install in debian jessie due to libpeg8 being removed.

RUN  apt-get update -q && apt-get install -qy gnupg ca-certificates \
  && echo 'debconf debconf/frontend select Noninteractive' | debconf-set-selections \
  && apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys 2009837CBFFD68F45BC180471F4F90DE2A9B4BF8 \
  && echo "deb https://apt.sonarr.tv/debian buster main" | tee /etc/apt/sources.list.d/sonarr.list \
  && apt-get update -q \
  && apt-get install -qy sonarr mediainfo curl \
  && apt-get clean \
  && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

ADD ./start.sh /NzbDrone/start.sh

EXPOSE 8989
EXPOSE 9898
VOLUME /volumes/config
VOLUME /volumes/completed
VOLUME /volumes/media

ENTRYPOINT ["/NzbDrone/start.sh"]
