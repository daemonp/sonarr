#!/bin/bash

set -e

function handle_signal {
  PID=$!
  echo "received signal. PID is ${PID}"
  kill -s SIGHUP $PID
}

trap "handle_signal" SIGINT SIGTERM SIGHUP

echo "starting sonarr"

mono /usr/lib/sonarr/bin/Sonarr.exe --no-browser -data=/data/config/sonarr & wait

echo "stopping sonarr"
